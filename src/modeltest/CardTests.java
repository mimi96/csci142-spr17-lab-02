package modeltest;

import model.Card;
import model.CardSuit;
import model.CardType;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Card;
/**
 * Test the functionality of the Card class
 * 
 * Date : 2/24/16
 * 
 * @author Spring 2016 class
 *
 */
public class CardTests
{
	private Card myCard;

	@Before
	public void setup()
	{
		myCard = new Card(CardSuit.HEARTS, CardType.QUEEN, null);

	}

	/**
	 * Test to see if the card has been correctly flipped
	 */
	@Test
	public void testIfFlipped()
	{

		boolean test = myCard.isFaceUp();
		myCard.flip();
		
		/**i had to change this to asserFalse from assertTrue because otherwise 
		it seemed to contradict itself.
		once the card is faced up and then flipped it means it must be not face up,
		with assertTrue it meant that if it was face up and then flipped it will still
		be true that it is still face up, which it is not. 
		*/
		assertFalse("The card was not flipped", test == myCard.isFaceUp());
	}
	
	

}
