package model;

public class PokerGame {

	public static void main(String[] args) 
	{
		System.out.println("Welcome to Poker!");
		
		/**
		 * I created this in order to make sure my shuffle works and it does. 
		 * I wanted to see if it was the code I wrote that did not pass the tests. 
		 * I think I may have a bug I don't see. 
		 * It says I have a Null Pointer but I made sure all my variables are initialized and set to point to some value; 
		 */
		
		Deck deck = new Deck();
		
		deck.constructDeck();
		
		deck.shuffle();
		
		System.out.println(deck);

	}

}
