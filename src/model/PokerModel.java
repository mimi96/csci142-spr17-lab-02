package model;


public class PokerModel
{
	private Player[] myPlayer;
	private int myIndexPlayerup;
	private int myMaxRounds;
	private int myRound;
	private Deck myDeck;

	public PokerModel(Player player)
	{
		myPlayer = new Player[2];
		ComputerPlayer CompPlayer = new ComputerPlayer("Mimi");
		myDeck = new Deck();
		myDeck.shuffle();
		myIndexPlayerup = 0;
		myPlayer[0] = player;
		myPlayer[1] = CompPlayer;
		myRound = 0;
		myMaxRounds=2;
	}

	
	public int switchTurns()
	{	
		if(myIndexPlayerup < myPlayer.length-1)
		{
			return myIndexPlayerup ++;
			
		}
		else
		{
			return myIndexPlayerup = 0;
		}
	}

	public void dealCards()
	{
		/**
		 * deal cards 
		 */
		
		
		for (int i = 0; i < 5; i++)
		{
			myPlayer[0].getHand().add(myDeck.draw());
			
			myPlayer[1].getHand().add(myDeck.draw());
		}
//		
	}

	public Player determineWinner()
	{
		return null;
	}

	public boolean resetGame()
	{	
		/**
		 *  To reset game just make sure you begin with a new deck, make your deck new again
		 *  
		 *  remove all cards from the hands. Clear them of the values they had 
		 *  
		 *  if deck is new and cards are removed from hands of each player then the game has been reset. 
		 */
		
		
		myDeck= new Deck();
		
		myDeck.shuffle();
		
		myPlayer[0].getHand().getCards().removeAllElements();
		myPlayer[1].getHand().getCards().removeAllElements();
		
		if (myPlayer[0].getHand().getNumberCardsInHand() == 0 
		 && myPlayer[1].getHand().getNumberCardsInHand() == 0)
		{
			return true;
		}
		else{return false;}
	}
	

	public Player getPlayerUp()
	{
		return myPlayer[myIndexPlayerup];
	}

	public Player getPlayer(int index)
	{
		return myPlayer[index];
	}

	public int getIndexPlayerUp()
	{
		return myIndexPlayerup;
	}

}
