package model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class Hand 
{
	private Vector<Card> myCards;
	int cardsMax;
	

	public Hand(int maxNum)
	{
		//setup a hand of cards with max number of 5 cards 
		cardsMax = maxNum;
		myCards = new Vector<Card>(maxNum);
		
		
	}

	public Vector<Card> getCards()
	{
		return myCards;
	}

	public int getNumberCardsInHand()
	{
		return getCards().size();
	}

	public boolean add(Card card)
	{
		/**
		 *  if I have cards less than 5 in the hand then I need to add a card
		 *  
		 *  Initially I used while but it made no sense because ultimately it
		 *  just kept adding the same card almost recursively. 
		 */
		
		
		if (myCards.size() < 5 )
		{
		return	myCards.add(card);	
		}
		return false;
	}
	
	

	public Vector<Card> discard(Vector<Integer> indices)
	{
		 Vector<Card> discartedCards = new Vector<Card>();
		 
		 if(!indices.isEmpty() && indices.size() < 4) 
		 {
			 
			 /**
			  * you mentioned in class to use reverse sorting to discard cards
			  * So I used collections to get the reverse order. 
			  */
			 
			 Collections.reverse(indices);
		 
			 for (int elements : indices)
			 {
				 if (elements == myCards.size() - 1)
				 {
					 	discartedCards.addElement(myCards.lastElement());
				 }
				 else
				 {
					 discartedCards.addElement(myCards.get(elements));
				 }
				 myCards.remove(elements);
			 }
		 }
		 
		 return discartedCards;
	}

	public String toString()
	{
		return "The hand is: " + myCards;
	}

	public void orderCards()
	{   
		/**
		 * had to create a new method in Card to be able to make this work.
		 */
		
		Collections.sort(myCards, new Comparator<Card>()
				{

					@Override
					public int compare(Card o1, Card o2)
					{
						return o1.compareTo(o2);
					}
			
				});
	}

}
