package model;


import java.util.Collections;
import java.util.Vector;

public class Deck
{
	//instance vars
	private Vector<Card> myCards;
	private final int myFullDeckSize;
	
	//construct
	public Deck()
	{
		myFullDeckSize = 52;
		myCards = new Vector<Card>(52);
		constructDeck();
		
	}
	
	

	public boolean constructDeck()
	{
		//generate cards
		
		for(CardSuit cardSuit : CardSuit.values())
		{
			for (CardType cardType : CardType.values())
			{
				//Add new cards to deck
				myCards.add(new Card(cardSuit, cardType, null));
			}
		}
		
		if (myCards.size() == 52)
		{
			return true;
		
		}
		else
		{
			return false;
		}
	} 
	
	

	public Card draw()
	{
		/**
		 * draw card from top of deck meaning it is the last element
		 * If it has been removed it means it has been drawn
		 * 
		 */
		if (myCards.size() > 0)
	{
		Card drawn = myCards.lastElement();
		myCards.remove(drawn);
		return drawn;
	}
	return null;
		
	}
	
	public boolean shuffle()
	{
		/**
		 * Shuffle using collections 
		 * 
		 * I could not figure out why my deck could not shuffle 
		 */
		
		
		Vector<Card> newCards = new Vector<Card>();
		myCards = new Vector<Card>(myFullDeckSize);
		newCards = myCards;
		Collections.shuffle(myCards);
	    
	    if (myCards.equals(newCards))
		{
			return false;
		}
		else
			return true;
	}

	public String toString()
	{
		
		String cards = "";
		
		int i = 0;
		
		for(Card aCard : this.myCards)
		{
			cards += "\n" + i + "-" + aCard.toString();
			i++;
		}
		return cards;
		
	
	}

	public Object clone()
	{
		return null;
	}
}
