package model;

import java.awt.Image;

public class Card
{

	private Image myImage;
	private boolean myIsFaceUp = false;
	
	private final CardSuit theSuit;
	private final CardType theType;
	private final Image Image;
	

	public Card(CardSuit suit, CardType type, Image image)
	{
		this.theSuit = suit; 
		this.theType = type;
		this.Image = image;
		
	}

	public boolean isFaceUp()
	{
		return myIsFaceUp;
	}

	public void flip()
	{
		myIsFaceUp = !myIsFaceUp; //flip card 
		
	}

	public int getType()
	{
		return theType.getType();
	}

	public String getSuit()
	{
		return theSuit.getSuit();
	}

	public Image getImage()
	{
		return Image;
	}

	public String toString()
	{
		return this.theSuit.toString() + "-" + this.theType.toString();
	}

	public Object clone()
	{
		Object newClone = new Card(theSuit, theType, Image);
		return newClone;
	}
	
	public int compareTo(Card card)
	{
		if(theType.getType() > card.getType())
		{
			return 1;
		}
		if(theType.getType() == card.getType())
		{
			return 0;
		}
		return -1;
	}

}
