package model;

public class Player
{
	private static final String DEFAULT_NAME = "John Cena";
	// http://regexr.com/ don't forget *$
	private static final String NAME_REGEX = "\\w*\\s?\\w*\\s?\\w*";
	private String myName;
	private int myNumberWins;
	protected boolean myAmAI; //changed it to protected from private to use it in CompPlayer
	protected Hand myHand;

	public Player(String name)
	{
		myName = name;
		myHand = new Hand(5);
	}

	public boolean validateName(String name)
	{
		if(name != myName)
		{
			return true;
		}
		return false;
		
		
	}

	public int incrementNumberWins()
	{
		for(int i= 0; i < myNumberWins; i++ );
		{
		myNumberWins ++;
		}
		return myNumberWins; 
	}

	public String toString()
	{
		return null;
	}

	public Object clone()
	{
		return null;
	}

	public Hand getHand()
	{
		return myHand;
	}

	public String getName()
	{
		return null;
	}

	public int getNumberWins()
	{
		return myNumberWins;
	} 

	public boolean getAmAI()
	{	
		return myAmAI;
	}

}
