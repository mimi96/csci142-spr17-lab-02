package model;

import java.util.Vector;

public class PokerHand extends Hand 
{

	private int myNumberCards;
	private int myMaxNumberCards;
	private Hand myHand;


	public PokerHand(int maxNum)
	{
		/**
		 * super always goes first 
		 * 
		 */
		super(maxNum);
		myMaxNumberCards = maxNum;
	}

	public PokerHandRanking determineRanking()
	{
		
		/**
		 * Order the ranks from highest rank to lowest rank which is the High Card 
		 */
		
		if (isRoyalFlush())
		{
			return PokerHandRanking.ROYAL_FLUSH;
		}
		if(isStraightFlush())
		{
			return PokerHandRanking.STRAIGHT_FLUSH;
		}
		if(isFourOfKind())
		{
			return PokerHandRanking.FOUR_OF_KIND;
		}
		if(isFullHouse())
		{
			return PokerHandRanking.FULL_HOUSE;
		}
		if(isFlush())
		{
			return PokerHandRanking.FLUSH;
		}
		if(isStraight())
		{
			return PokerHandRanking.STRAIGHT;
		}
		if(isThreeOfKind())
		{
			return PokerHandRanking.THREE_OF_KIND;
		}
		if(isTwoPair())
		{
			return PokerHandRanking.TWO_PAIR;
		}
		if(isPair())
		{
			return PokerHandRanking.PAIR;
		}
		else
		return PokerHandRanking.HIGH_CARD;
	}

	public int compareTo(PokerHand pokerHand)
	{
	
		  
		  if (getRanking() > pokerHand.getRanking())
		  {
			  return 1;
		  }
		  if (getRanking() < pokerHand.getRanking())
		  {
			  return -1;
		  }
		  else
		  {
			  return 0;
		  }
	}

	public String toString()
	{
		return null;
	}

	public int getRanking()
	{
	return determineRanking().getRank();
		
	}

	public int getNumberCards()
	{
		return myNumberCards;
	}

	public int getMaxNumberCards()
	{
		return myMaxNumberCards;
	}
	
	
	
	/**
	 *  The code below will need to set the rules for each Rank (hand) in poker 
	 */
	
	
	
	
	public boolean isHighCard()
	{
		/** 
		 * If you have not made any hands you play high card
		 * So if no pair, no twoPair, no royal flush.... you play HighCard
		 * 
		 */
		
		
		return true;
	}

	public boolean isPair()
	{
		 /**
		  * Example [A][A][8][4][7] of a Pair card 
		  *  
		  *  Two cards of the same type 
		  */
		
		
		orderCards();
		
		Vector<Card>  newDeck = getCards();
		
		for(int i = 1; i < newDeck.size() -2; i++)
		{
			if (newDeck.get(i).getType() == newDeck.get(i+1).getType())
			{
				return true;
			}
		}
		
		return false;
	}

	public boolean isTwoPair()
	{
		/**
		 * Example [4][4][3][3][Q] of Two Pair 
		 * 
		 * Two different pairs 
		 */
		
		int indexNum =0;
		
		orderCards();
		
		Vector<Card>  newDeck = getCards();
		
		
		for (int i= 0; i < newDeck.size() - 2; i++ )
		{
			if(newDeck.get(i).getType() == newDeck.get(i+1).getType())
			{
				indexNum++;
			}
		}
		if(indexNum ==2)
		{
			return true;
		}
		else return false;
		
	}

	public boolean isThreeOfKind()
	{
		/**
		 *  [7][7][7][6][5]
		 *  
		 *  Three cards of the same rank
		 * 
		 */
		
		
		orderCards();
		
		Vector <Card> newDeck = getCards();
		
		getCards();
		for (int i = 0 ; i < newDeck.size() -2; i++)
		{
			if(newDeck.get(i).getType()==newDeck.get(i+1).getType() 
			&& newDeck.get(i+1).getType() == newDeck.get(i+2).getType())
			{
				return true;
			}
		}
		return false;
	}
	
	

	public boolean isStraight()
	{
		/**
		 *  [9][8][7][6][5]
		 *  
		 *  NOT SAME SUIT, five cards in a sequence
		 * 
		 */
		
		
		
		return true;
	}

	public boolean isFlush()
	{
		
		/**
		 *  [4][7][J][2][5]
		 *  
		 *  SAME SUIT, not sequence
		 * 
		 */
		
		
		
		orderCards();
		
		Vector <Card> newDeck = getCards();
		
		for(int i = 0; i < newDeck.size() - 1; i++)
		{
			if(newDeck.get(i).getSuit() != newDeck.get(i+1).getSuit() 
			|| newDeck.get(i).getType()+1 == newDeck.get(i+1).getType())
			{
				return false;
			}
		}
		
		return true;
		
	}

	public boolean isFullHouse()
	{
		/**
		 *  [7][7][7][6][6]
		 *  
		 *  Three of same type, two pair 
		 * 
		 */
		
		
		
		return false;
		
	}

	public boolean isFourOfKind()
	{
		/**
		 *  [7][7][7][7][5]
		 *  
		 *  4 cards of the same rank
		 * 
		 */
		
		
		
		return false;
	}

	public boolean isStraightFlush()
	{
		/**
		 *  [9][8][7][6][5]
		 *  
		 *  SAME SUIT, in sequence
		 * 
		 */
		
		
		//return (isStraight() && isFlush());
	return false;
	}

	public boolean isRoyalFlush()
	{
		
		/**
		 *  [10][J][Q][K][A]
		 *  
		 *  same suit, 10, J, Q, K, A
		 * 
		 */
		
		
		return false;
	}

}
